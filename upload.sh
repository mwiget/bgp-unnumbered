#!/bin/bash

#for vmx in spine1 spine2 leaf1; do
for vmx in leaf1 spine1 spine2; do
  IP=$(docker-compose logs $vmx |grep 'password '|cut -d\( -f2|cut -d\) -f1)
  echo "$vmx ($IP)..."
  scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ad_bgp_peers.slax $IP:/var/db/scripts/op/
done

